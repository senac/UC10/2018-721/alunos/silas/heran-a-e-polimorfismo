/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerente;

/**
 *
 * @author sala304b
 */
public class AssistenteAdministrativo extends Assistente {

    private String turno;
    private final double adicionalNoturno = 0.25;
    
    
    
    public AssistenteAdministrativo(String numeroDeMatricula, String turno) {
        super(numeroDeMatricula);
        this.turno = turno;
    }
    
    public double getSalario(){
        if(this.turno.equalsIgnoreCase("Noturno")){
            return super.getSalario() + ( super.getSalario() * adicionalNoturno;
        } else {
            return super.getSalario();
        }
    }
}
