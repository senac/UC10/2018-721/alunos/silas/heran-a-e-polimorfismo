/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerente;

/**
 *
 * @author sala304b
 */
public class AssistenteTecnico extends Assistente {

   private double bonus;

    public AssistenteTecnico(String matricula) {
        super(matricula);
    }

    public AssistenteTecnico(String matricula, double bonus) {
        super(matricula);
        this.bonus = bonus;
    }

    @Override
    public double getSalario() {

        return super.getSalario() + this.getBonus();

    }

    public double getBonus() {
        return bonus;
    }

}
