/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conta;

/**
 *
 * @author sala304b
 */
public class ContaCorrente extends Conta {
    
   
    private double chequeEspecial;

    public ContaCorrente() {
        super(1);
    }

    public ContaCorrente(double taxa) {
        super(taxa);

    }

    public ContaCorrente(double taxa, double chequeEspecial) {
        super(taxa);
        this.chequeEspecial = chequeEspecial;
    }

   
    @Override
    public void sacar(double valor) {
        if (getSaldo() >= valor + this.taxa) {
            this.saldo -= valor;
            this.saldo -= taxa;
        } else {
            throw new RuntimeException("Saque excede limite");
        }

    }


    public double getSaldo() {
        return this.saldo + this.chequeEspecial;
    }

    public double getChequeEspecial() {
        return chequeEspecial;
    }

    public void setChequeEspecial(double chequeEspecial) {
        this.chequeEspecial = chequeEspecial;
    }
    
    
   
}
