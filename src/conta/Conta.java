/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conta;

import java.text.NumberFormat;


/**
 *
 * @author sala304b
 */
public abstract class Conta {
    
    protected double saldo;
    protected double taxa;
    
    public Conta(double taxa){
        this.taxa = taxa;
    }
    
    public double getsaldo(){
        return saldo;
    }
    
    public void saldo (double saldo){
        this.saldo = saldo;
    }

    public abstract void sacar(double valor);
    
    public void depositar(double valor){
        this.saldo += valor;
    }

    public void saldoImpresso(){
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        System.out.println(nf.format(this.getsaldo()));
    }

    
}
