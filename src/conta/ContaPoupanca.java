/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conta;

/**
 *
 * @author sala304b
 */
public class ContaPoupanca extends Conta{
    
    public ContaPoupanca() {
        super(0);
    }

    @Override
    public void sacar(double valor) {
       if(this.getsaldo() >= valor ){
            this.saldo -= valor ;
        }else{
            throw new RuntimeException("Saque excede limite") ; 
        }
    }
}

